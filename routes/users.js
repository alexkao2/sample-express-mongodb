var express = require('express');
var router = express.Router();

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
//var mongo = require('mongoskin');
//var db = mongo.db("mongodb://localhost:27017/nodetest2", {native_parser:true});

    db.collection('userlist').find().toArray(function (err, items) {
        res.json(items);
    });

});

module.exports = router;
